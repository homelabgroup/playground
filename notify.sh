#!/bin/bash

MESSAGE=$1
DISCORD_WEBHOOK=$2

curl -s -i -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "{\"content\": \"${MESSAGE}\"}" "${DISCORD_WEBHOOK}"
